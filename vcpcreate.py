import argparse
import boto3

def create_vpc_and_resources(ec2_client, friendly_name, vpc_cidr_block, subnet_cidr_block):
    print("Creating a new VPC:")

    vpc_response = ec2_client.create_vpc(CidrBlock=vpc_cidr_block)
    vpc_id = vpc_response['Vpc']['VpcId']

    ig_response = ec2_client.create_internet_gateway()
    ig_id = ig_response['InternetGateway']['InternetGatewayId']
    ec2_client.attach_internet_gateway(InternetGatewayId=ig_id, VpcId=vpc_id)

    rt_response = ec2_client.create_route_table(VpcId=vpc_id)
    rt_id = rt_response['RouteTable']['RouteTableId']
    ec2_client.create_route(
        DestinationCidrBlock='0.0.0.0/0',
        GatewayId=ig_id,
        RouteTableId=rt_id
    )

    subnet_response = ec2_client.create_subnet(VpcId=vpc_id, CidrBlock=subnet_cidr_block)
    subnet_id = subnet_response['Subnet']['SubnetId']
    ec2_client.associate_route_table(RouteTableId=rt_id, SubnetId=subnet_id)

    ec2_client.create_tags(Resources=[vpc_id, ig_id, rt_id, subnet_id], Tags=[{'Key': 'Name', 'Value': friendly_name}])

def main():
    parser = argparse.ArgumentParser(description="AWS VPC managen")
    parser.add_argument("friendly_name", help="Friendly name")
    parser.add_argument("vpc_cidr_block", help="CIDR block for the new VPC")
    parser.add_argument("subnet_cidr_block", help="CIDR block for the new subnet")
    parser.add_argument("--aws-access-key", help="AWS Access Key ID")
    parser.add_argument("--aws-secret-key", help="AWS Secret Access Key")
    parser.add_argument("--region", default="us-east-1", help="AWS region (default: us-east-1)")
    args = parser.parse_args()

    session_args = {
        "region_name": args.region
    }

    if args.aws_access_key and args.aws_secret_key:
        session_args["aws_access_key_id"] = args.aws_access_key
        session_args["aws_secret_access_key"] = args.aws_secret_key
        session = boto3.Session(**session_args)
    ec2_client = session.client('ec2')

    create_vpc_and_resources(ec2_client, args.friendly_name, args.vpc_cidr_block, args.subnet_cidr_block)

if __name__ == "__main__":
    main()
